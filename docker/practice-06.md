# Practice 06

> This is the most easy solution that I could develop for this practice;
> I know there are some others ways to solve this task, so just wanted to make it simple.

> Of course you can modify it and propose different alternatives :) 

### Video Tutorial ( no sound )
[Running this practice step by step](./Practice06.mp4)

---
## Docker File
```python
FROM ubuntu:latest
#  ---------- System Settings -------------
# Define Working Directory
WORKDIR /sw
# Update all the references to Ubuntu repositories
RUN apt-get update
#  ----------   PYTHON -------------
# Install Python, PIP and create links 
RUN apt-get install -y python3-pip python3-dev
RUN cd /usr/local/bin
RUN ln -s /usr/bin/python3 python
RUN pip3 install --upgrade pip
#  ---------- AWS CLI  -------------
RUN pip3 install --upgrade awscli
#  ---------- TERRAFORM 12  -------------
# Install wget and unzip as helpers 
# We could avoid this step and unzip the file
# Then use COPY to include the unzipped content
# and reduce the image size.
RUN apt-get install wget -y
RUN apt-get install unzip -y
# Get Terraform installer from hashicorp repository
RUN wget --quiet https://releases.hashicorp.com/terraform/0.12.30/terraform_0.12.30_linux_amd64.zip
# Unzip and install 
RUN unzip terraform_0.12.30_linux_amd64.zip
RUN mv terraform /usr/bin
# Remove the zip 
# also we could uninstall unzip and wget commands just for fun 
RUN rm terraform_0.12.30_linux_amd64.zip
```
### Build a new image:
```docker
docker build -t aws-cicd -f Dockerfile .
docker images 
```
### Tag the new image 
```docker
docker tag aws-cicd franciscogil/practice06:aws-cicd
```
### Login to Docker Hub  ( use your own account )
```docker
docker login --username=franciscogil
password:   [ TYPE HERE YOUR PASSWORD ]
```
### Push Image to repository
```docker
docker push franciscogil/practice06:aws-cicd
```

