# Mysql Container 

## pull mysql image from docker hub

```bash
docker pull mysql:latest
```


## create an instance ( container ) of mysql make sure the data directory is created previously
```bash
docker run --name mysqlDemo -e MYSQL_ROOT_PASSWORD=password -p 3306:3306 -v /data:/var/lib/mysql -d  mysql:latest
            [Container Name]   [Environment Variable]      [port out:in] [vol  out:in]          [bg] [image:tag]
```

## Access the new created container and use the mysql credentials for root user:
```bash
docker exec -it mysqlDemo mysql -uroot -p
Enter password: password
```

## Create a new DB and Table as below
```sql
mysql>show databases;
mysql>create database demo;
mysql>create table demo.table1 ( column1 varchar(250), column2 varchar(250) );
mysql>use demo;
mysql>show tables;
mysql>select * from demo.table1;
```

## Get the IPAddress to be used for the python demo 

```bash
docker inspect mysqlDemo | grep -i ip
```

--- 
# Python Container

## Create requirements file with the content below

```python
mysql-connector-python
```

## Create demo.py with the content below

```python
import mysql.connector

mysql_db_config = {
	'host': ' ---> IP FROM MYSQL CONTAINER <--- ',
	'user': 'root',
	'password': 'password',
	'database': 'demo'
}


mysqlConnectionInstance = mysql.connector.connect(**mysql_db_config)
cursor = mysqlConnectionInstance.cursor()
sqlInsert = "INSERT INTO demo.table1 (column1, column2) VALUES (CURRENT_TIMESTAMP(), 'Hello' ) "
cursor.execute(sqlInsert)
mySQLConn.commit()
cursor.close()
```

--- 

# Docker File

```docker
FROM python:3.6
WORKDIR /app
COPY requirements .
COPY demo.py .
RUN pip install -r requirements
CMD python demo.py
```

## Build a new image from python 3.6 using the dockerfile created Previously

```bash
docker build -t pythondemo -f Dockerfile .
```
## run the python container 
```bash
docker run --name pythondemo1 -it -d pythondemo
```

## Confirm the data has been inserted
```bash
docker exec -it mysqlDemo mysql -uroot -p
Enter password: password

mysql>use demo;
mysql>select * from demo.table1;
```

### Very Basic Diagram jusr for fun

> Diagram 1
![Diagram](./d1.PNG)

# Enjoy!
